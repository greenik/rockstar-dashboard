function secondsToString(s) {
    let uptimeString = '';
    let days = Math.floor(s / 60 / 60 / 24);
    let hours = Math.floor(s / 60 / 60) % 24;
    let minutes = Math.floor(s / 60) % 60;
    if(days) {
        uptimeString += `${days}d `
    }
    if(hours) {
        uptimeString += `${hours}h `
    }
    if(minutes) {
        uptimeString += `${minutes}m `
    }
    return uptimeString;
}

$.get('http://preview.rockstardevelopers.de/dashboard/api/v1/devices', function(devices) {
    devices = devices.map(function(device) {
        let mainStatuses = [];
        device.status.forEach(function(status){
            if (status.key.key.indexOf('cpu') !== -1) {
                mainStatuses.push({
                    'name': 'CPU',
                    'isOk': parseFloat(status.value) < parseFloat(status.max)
                })
            }
            if (status.key.key.indexOf('mem') !== -1) {
                mainStatuses.push({
                    'name': 'MEM',
                    'isOk': parseFloat(status.value) < parseFloat(status.max)
                })
            }
            if (status.key.key.indexOf('disk') !== -1) {
                mainStatuses.push({
                    'name': 'DISK',
                    'isOk': parseFloat(status.value) < parseFloat(status.max)
                })
            }
        });
        device.mainStatuses = mainStatuses;
        device.uptimeString = secondsToString(device.uptime_seconds);
        return device;
    });
    Templates.List.render('#app', {
        devices: devices
    });

    $('.device-card').click(function(event){
        let deviceId = $(event.currentTarget).data('id');
        let clickedDevice;
        for(let i = 0; i<devices.length; i++) {
            if(devices[i].id === deviceId) {
                clickedDevice = devices[i];
                break;
            }
        }
        console.log(clickedDevice);
        let html = ``;
        for(let i = 0; i<clickedDevice.status.length; i++) {
            html += `<div>${clickedDevice.status[i].key.key} - ${clickedDevice.status[i].value} ${clickedDevice.status[i].key.unit}</div>`
        }
        $('.device-details-body').html(html);
        $('#device-details').show();
    });
    $('.close-details').click(function(){
        $('#device-details').hide();
    });
});

Templates.Datetime.render('#datetime', {
    time: moment().format('h:mm:ss a'),
    date: moment().format('DD.MM.YYYY')
});
